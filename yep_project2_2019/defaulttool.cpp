#include "defaulttool.h"

defaultTool::defaultTool()
{

}

void defaultTool::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    (void)x;
    (void)y;
    (void)image;
    (void)colorLayer;
}

void defaultTool::getToolBox(QFormLayout *layout)
{
    removeOldToolBox(layout);
}

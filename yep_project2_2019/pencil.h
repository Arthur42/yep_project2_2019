#ifndef PENCIL_H
#define PENCIL_H

#include "paintbrush.h"
#include <QPushButton>
#include <QCheckBox>

class Pencil : public PaintBrush
{
    Q_OBJECT

public:
    Pencil();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) final;
    void getToolBox(QFormLayout *layout) final;

private slots:
    void on_check_clicked(int state);

private:
    void print_square(QImage *image, int x, int y);
    void print_circle(QImage *image, int x, int y);
    bool _square; // if square == 1 the square forme will be use else the circle will be use
    QCheckBox *_boxCheck;
};

#endif // PENCIL_H

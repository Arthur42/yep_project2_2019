#ifndef DATASAVER_H
#define DATASAVER_H

#include <iostream>

class DataSaver
{
public:
    DataSaver();
    void setTheme(int);
    int getTheme();
private:
    void saveDataToFile();
    int getDataFromFile(std::string);
    int _theme;
};

#endif // DATASAVER_H

#ifndef MENURESIZEWINDOW_H
#define MENURESIZEWINDOW_H

#include <QDialog>
#include <QSize>

namespace Ui {
class menuResizeWindow;
}

class menuResizeWindow : public QDialog
{
    Q_OBJECT

public:
    explicit menuResizeWindow(int, int, QWidget *parent = nullptr);
    ~menuResizeWindow();
    QSize getSize();

private slots:
    void on_yes_clicked();

    void on_no_clicked();

private:
    Ui::menuResizeWindow *ui;
    int _w;
    int _h;
};

#endif // MENURESIZEWINDOW_H

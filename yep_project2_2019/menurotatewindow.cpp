#include "menurotatewindow.h"
#include "ui_menurotatewindow.h"

menuRotateWindow::menuRotateWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuRotateWindow)
{
    ui->setupUi(this);
    _angle = 0;
}

menuRotateWindow::~menuRotateWindow()
{
    delete ui;
}

int menuRotateWindow::getAngle()
{
    return _angle;
}

void menuRotateWindow::on_yes_clicked()
{
    _angle = std::stod(ui->angle->text().toStdString());
    close();
}

void menuRotateWindow::on_no_clicked()
{
    _angle = 0;
    close();
}

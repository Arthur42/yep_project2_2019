#include "layer.h"

Layer::Layer()
{
    _name = "";
    _isTransparent = true;
    _fillColor = QColor(Qt::transparent);
    _locked = false;
}

Layer::Layer(QString &name, bool isTransparent, QColor &color):
    _name(name), _isTransparent(isTransparent), _fillColor(color)
{}

QString Layer::getName() const
{
    return _name;
}

void Layer::setName(const QString &name)
{
    _name = name;
}

bool Layer::isTransparent() const
{
    return _isTransparent;
}

void Layer::setTransparent(const bool &transparent)
{
    _isTransparent = transparent;
}

QColor Layer::getFillColor() const
{
    return _fillColor;
}

void Layer::setFillColor(const QColor &color)
{
    _fillColor = color;
}

QScrollArea *Layer::getScrollArea() const
{
    return _area;
}

void Layer::setScrollArea(QScrollArea *area)
{
    _area = area;
}

MyQLabel *Layer::getLabel() const
{
    return _label;
}

void Layer::setLabel(MyQLabel *label)
{
    _label = label;
}

bool Layer::isLocked() const
{
    return _locked;
}

void Layer::setLocked(const bool &status)
{
    _locked = status;
}

QImage *Layer::getImage()
{
    return _image;
}

void Layer::setImage(QImage *image)
{
    _image = image;
}


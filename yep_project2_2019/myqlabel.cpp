#include "myqlabel.h"

MyQLabel::MyQLabel(QWidget* parent, Qt::WindowFlags f): QLabel(parent, f)
{

}

MyQLabel::~MyQLabel() {}

void MyQLabel::mousePressEvent(QMouseEvent* event) {
    emit clicked(event);
}

void MyQLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    emit release();
}

void MyQLabel::mouseMoveEvent(QMouseEvent *ev)
{
    emit moved(ev);
}

#ifndef ERASER_H
#define ERASER_H

#include "atool.h"
#include <QPainter>
#include <QLineEdit>
#include <QCheckBox>
#include <QIntValidator>

class Eraser : public ATool
{
    Q_OBJECT

public:
    Eraser();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) final;
    void getToolBox(QFormLayout *layout) final;

private slots:
    void on_size_editingfinished();
    void on_check_stateChange(int status);

private:
    void erase_circle(QImage *image, int x, int y, const QColor &colorLayer);
    void erase_square(QImage *image, int x, int y, const QColor &colorLayer);
    QLineEdit *_boxSize;
    QCheckBox *_boxCheck;
    bool _square;
};

#endif // ERASER_H

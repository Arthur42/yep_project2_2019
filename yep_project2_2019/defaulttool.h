#ifndef DEFAULTTOOL_H
#define DEFAULTTOOL_H

#include "atool.h"

class defaultTool: public ATool
{
public:
    defaultTool();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) final;
    void getToolBox(QFormLayout *layout) final;
};

#endif // DEFAULTTOOL_H

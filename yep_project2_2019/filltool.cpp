#include "filltool.h"

FillTool::FillTool()
{
}

FillTool::~FillTool()
{}

void FillTool::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    _colorToReplace = image->pixelColor(x, y);
    _imageWidth = image->width();
    _imageHeight = image->height();
    if (_colorToReplace != _color)
        fillPixelCol(image, 0);
}

void FillTool::getToolBox(QFormLayout *layout)
{
    _boxColor = new QPushButton();

    removeOldToolBox(layout);

    QColor c = _color;
    _boxColor->setText("");
    _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(c.red()) + ", " + std::to_string(c.green()) + ", " + std::to_string(c.blue()) + ", " + std::to_string(c.alpha()) + ")").c_str());
    connect(_boxColor, SIGNAL(clicked()), this, SLOT(on_color_clicked()));
    layout->addRow("Couleur", _boxColor);
}

void FillTool::on_color_clicked()
{
    QColor c = QColorDialog::getColor(_color, this, "Choose a color");

    if (c.isValid()) {
        _color = c;
        _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(c.red()) + ", " + std::to_string(c.green()) + ", " + std::to_string(c.blue()) + ", " + std::to_string(c.alpha()) + ")").c_str());
    }
}

void FillTool::fillPixelCol(QImage *image, int y)
{
    for (int x = 0; x <= _imageWidth; x++)
        fillPixelLine(image, x, y);
    if (y < _imageHeight)
        fillPixelCol(image, y + 1);
}

void FillTool::fillPixelLine(QImage *image, int x, int y)
{
    if ((_colorToReplace == image->pixelColor(x, y))) {
        image->setPixelColor(x, y, _color);
    }
}

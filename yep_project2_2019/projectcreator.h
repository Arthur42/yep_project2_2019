#ifndef PROJECTCREATOR_H
#define PROJECTCREATOR_H

#include <QDialog>
#include <QIntValidator>
#include <QColorDialog>
#include <QMessageBox>
#include <string>

#include "project.h"

namespace Ui {
class ProjectCreator;
}

class ProjectCreator : public QDialog
{
    Q_OBJECT

public:
    explicit ProjectCreator(QWidget *parent = nullptr);
    ~ProjectCreator();
    Project getProject() const;
    bool cancelled() const;


private slots:
    void on_projectName_editingFinished();

    void on_projectWidth_editingFinished();

    void on_projectHeight_editingFinished();

    void on_projectTransparency_stateChanged(int arg1);

    void on_projectColor_clicked();

    void on_endCreator_clicked();

    void on_cancelButton_clicked();

    void on_ProjectCreator_finished(int result);

private:
    Ui::ProjectCreator *ui;
    Project _project;
    bool _cancelled;

};

#endif // PROJECTCREATOR_H

#include "menuaboutwindow.h"
#include "ui_menuaboutwindow.h"

menuAboutWindow::menuAboutWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuAboutWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("A propos");
}

menuAboutWindow::~menuAboutWindow()
{
    delete ui;
}

void menuAboutWindow::on_pushButton_released()
{
    close();
}

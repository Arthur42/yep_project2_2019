#ifndef LAYER_H
#define LAYER_H

#include <QString>
#include <QColor>
#include <QImage>
#include <QScrollArea>

#include "myqlabel.h"

class Layer
{
public:
    Layer();
    Layer(QString &name, bool isTransparent, QColor &color);
    QString getName() const;
    void setName(const QString &name);
    bool isTransparent() const;
    void setTransparent(const bool &transparent);
    QColor getFillColor() const;
    void setFillColor(const QColor &color);
    QImage *getImage();
    void setImage(QImage *image);
    QScrollArea *getScrollArea() const;
    void setScrollArea(QScrollArea *area);
    MyQLabel *getLabel() const;
    void setLabel(MyQLabel *label);
    bool isLocked() const;
    void setLocked(const bool &status);

private:
    QString _name;
    bool _isTransparent;
    QColor _fillColor;
    QImage *_image;
    QScrollArea *_area;
    MyQLabel *_label;
    bool _locked;
};

#endif // LAYER_H

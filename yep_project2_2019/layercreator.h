#ifndef LAYERCREATOR_H
#define LAYERCREATOR_H

#include <QDialog>
#include <QColorDialog>
#include <QMessageBox>
#include "layer.h"

namespace Ui {
class LayerCreator;
}

class LayerCreator : public QDialog
{
    Q_OBJECT

public:
    explicit LayerCreator(QWidget *parent = nullptr);
    ~LayerCreator();
    Layer getLayer() const;
    bool wasCancelled() const;

private slots:
    void on_transparent_stateChanged(int arg1);

    void on_color_button_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();
    void closeEvent(QCloseEvent *event) final;

    void on_name_returnPressed();

    void on_name_editingFinished();

private:
    Ui::LayerCreator *ui;
    Layer _layer;
    bool _cancelled;
};

#endif // LAYERCREATOR_H

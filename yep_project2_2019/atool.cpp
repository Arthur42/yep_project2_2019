#include "atool.h"
#include "ui_mainwindow.h"

ATool::ATool()
{
    _size = 11;
    _color = Qt::black;
}

ATool::~ATool()
{}

void ATool::setSize(int size)
{
    _size = size;
}

int ATool::getSize() const
{
    return _size;
}

void ATool::setColor(QColor color)
{
    _color = color;
}

QColor ATool::getColor() const
{
    return _color;
}

void ATool::removeOldToolBox(QFormLayout *layout)
{
    QLayoutItem* item;
    while ((item = layout->takeAt(0)) != NULL) {
        delete item->widget();
        delete item;
    }
}

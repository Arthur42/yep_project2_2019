#include "layercreator.h"
#include "ui_layercreator.h"

#include <iostream>

LayerCreator::LayerCreator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LayerCreator)
{
    ui->setupUi(this);
    _cancelled = false;
}

LayerCreator::~LayerCreator()
{
    delete ui;
}

Layer LayerCreator::getLayer() const
{
    return _layer;
}

bool LayerCreator::wasCancelled() const
{
    return _cancelled;
}

void LayerCreator::on_transparent_stateChanged(int arg1)
{
    _layer.setTransparent((arg1 == 0) ? false : true);
    ui->color_button->setEnabled((arg1 == 0) ? true : false);
}

void LayerCreator::on_color_button_clicked()
{
    QColor c = QColorDialog::getColor(Qt::transparent, this, "Choose fill color");

    _layer.setFillColor(c);
    ui->color_button->setStyleSheet(
              std::string("background-color: rgba(" +
              std::to_string(c.red()) + ", " +
              std::to_string(c.green()) + ", " +
              std::to_string(c.blue()) + ", " +
              std::to_string(c.alpha()) + ")\
              ; border: 1px solid black").c_str());
}

void LayerCreator::on_pushButton_3_clicked()
{
    (_layer.getName() == "") ?
    QMessageBox::information(this, "Alert", "You must provide a name") :
    close();
}

void LayerCreator::on_pushButton_2_clicked()
{
    _cancelled = true;
    close();
}

void LayerCreator::closeEvent(QCloseEvent *event)
{
    (void)event;
    _cancelled = true;
}

void LayerCreator::on_name_returnPressed()
{
    _layer.setName(ui->name->text());
    ui->name->clearFocus();
}

void LayerCreator::on_name_editingFinished()
{
    _layer.setName(ui->name->text());
    ui->name->clearFocus();
}

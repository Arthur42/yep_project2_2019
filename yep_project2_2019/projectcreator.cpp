#include "projectcreator.h"
#include "ui_projectcreator.h"

ProjectCreator::ProjectCreator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectCreator)
{
    ui->setupUi(this);
    ui->projectWidth->setValidator(new QIntValidator(this));
    ui->projectHeight->setValidator(new QIntValidator(this));
    _cancelled = false;
}

ProjectCreator::~ProjectCreator()
{
    delete ui;
}

Project ProjectCreator::getProject() const
{
    return _project;
}

bool ProjectCreator::cancelled() const
{
    return _cancelled;
}

void ProjectCreator::on_projectName_editingFinished()
{
    QString str = ui->projectName->text();
    _project.setName(str);
}

void ProjectCreator::on_projectWidth_editingFinished()
{
    _project.setWidth(ui->projectWidth->text().toInt());
}

void ProjectCreator::on_projectHeight_editingFinished()
{
    _project.setHeight(ui->projectHeight->text().toInt());
}

void ProjectCreator::on_projectTransparency_stateChanged(int state)
{
    _project.setTransparent((state == 0) ? false : true);
    ui->label_4->setEnabled(!_project.isTransparent());
    ui->projectColor->setEnabled(!_project.isTransparent());
}

void ProjectCreator::on_projectColor_clicked()
{
    QColor c = QColorDialog::getColor(Qt::white, this, "Choose color background");

    _project.setColor(c);
    ui->projectColor->setStyleSheet(std::string("background-color: rgba(" +
                              std::to_string(c.red()) + ", " +
                              std::to_string(c.green()) + ", " +
                              std::to_string(c.blue()) + ", " +
                              std::to_string(c.alpha()) + "); border: 1px solid black").c_str());
}

void ProjectCreator::on_endCreator_clicked()
{
    (_project.getWidth() > 0 && _project.getHeight() > 0 && _project.getName() != "") ?
                close() :
                QMessageBox::information(this, "Error", "You must provide name AND size of the new project");
}

void ProjectCreator::on_cancelButton_clicked()
{
    _cancelled = true;
    close();
}

void ProjectCreator::on_ProjectCreator_finished(int result)
{
    (void)result;
    _cancelled = !(_project.getWidth() > 0 && _project.getHeight() > 0 && _project.getName() != "");
}

#ifndef MENUROTATEWINDOW_H
#define MENUROTATEWINDOW_H

#include <QDialog>

namespace Ui {
class menuRotateWindow;
}

class menuRotateWindow : public QDialog
{
    Q_OBJECT

public:
    explicit menuRotateWindow(QWidget *parent = nullptr);
    ~menuRotateWindow();
    int getAngle();

private slots:
    void on_yes_clicked();
    void on_no_clicked();

private:
    Ui::menuRotateWindow *ui;
    int _angle;
};

#endif // MENUROTATEWINDOW_H

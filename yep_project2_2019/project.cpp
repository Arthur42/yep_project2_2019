#include "project.h"

Project::Project()
{
    _name = "";
    _width = -1;
    _height = -1;
    _isTransparent = true;
    _color = QColor(Qt::transparent);
}

Project::Project(QString &name, int width, int height, bool isTransparent, QColor &color)
{
    _name = name;
    _width = width;
    _height = height;
    _isTransparent = isTransparent;
    _color = color;
}

QString Project::getName() const
{
    return _name;
}

void Project::setName(QString name)
{
    _name = name;
}

int Project::getWidth() const
{
    return _width;
}

void Project::setWidth(int width)
{
    _width = width;
}

int Project::getHeight() const
{
    return _height;
}

void Project::setHeight(int height)
{
    _height = height;
}

bool Project::isTransparent() const
{
    return _isTransparent;
}

void Project::setTransparent(bool status)
{
    _isTransparent = status;
    _color = (status) ? QColor(Qt::transparent) : _color;
}

QColor Project::getColor() const
{
    return _color;
}

void Project::setColor(const QColor &color)
{
    _color = color;
}

void Project::writeInJSon(QJsonObject &obj)
{
    QJsonObject background;

    obj["name"] = _name;
    obj["width"] = _width;
    obj["height"] = _height;
    obj["transparency"] = _isTransparent;
    background["red"] = _color.red();
    background["green"] = _color.green();
    background["blue"] = _color.blue();
    background["alpha"] = _color.alpha();
    obj["background"] = background;
}



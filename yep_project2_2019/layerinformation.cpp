#include "layerinformation.h"
#include "ui_layerinformation.h"

LayerInformation::LayerInformation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LayerInformation)
{
    ui->setupUi(this);
    _toDelete = false;
}

LayerInformation::LayerInformation(const Layer &layer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LayerInformation)
{
    ui->setupUi(this);
    ui->lineEdit->setText(layer.getName());
    _toDelete = false;
}

LayerInformation::~LayerInformation()
{
    delete ui;
}

bool LayerInformation::toDelete() const
{
    return _toDelete;
}

void LayerInformation::on_pushButton_2_clicked()
{
    QMessageBox::StandardButton btn = QMessageBox::question(this, "",
                                                            "Are you sure to delete this layer ?",
                                                            QMessageBox::Yes | QMessageBox::No);
    if (btn == QMessageBox::Yes)
        _toDelete = true;
    close();
}

void LayerInformation::on_pushButton_clicked()
{
    close();
}

#ifndef MENUABOUTWINDOW_H
#define MENUABOUTWINDOW_H

#include <QDialog>

namespace Ui {
class menuAboutWindow;
}

class menuAboutWindow : public QDialog
{
    Q_OBJECT

public:
    explicit menuAboutWindow(QWidget *parent = nullptr);
    ~menuAboutWindow();

private slots:
    void on_pushButton_released();

private:
    Ui::menuAboutWindow *ui;
};

#endif // MENUABOUTWINDOW_H

#ifndef MYQLABEL_H
#define MYQLABEL_H

#include <QLabel>

/*
 *  Class permettant de créer un Label cliquable
 *      Détection de clique
 *      Détection de fin de clique
 *      Détection de movement de la souris
 *  Toutes les fonctions sont appelées si elles sont réalisées au sein du label
 */

class MyQLabel : public QLabel
{
    Q_OBJECT
public:
    explicit MyQLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~MyQLabel();

    signals:
        void clicked(QMouseEvent *even);
        void release();
        void moved(QMouseEvent *ev);

    protected:
        void mousePressEvent(QMouseEvent* event) final;
        void mouseReleaseEvent(QMouseEvent *ev) final;
        void mouseMoveEvent(QMouseEvent *ev) final;
};

#endif // MYQLABEL_H

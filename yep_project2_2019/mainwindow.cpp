#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myqlabel.h"
#include "defaulttool.h"
#include "paintbrush.h"
#include "pencil.h"
#include "eraser.h"
#include "pickcolor.h"
#include "filltool.h"
#include "menuthemewindow.h"
#include "menuaboutwindow.h"
#include "menuresizewindow.h"
#include "menurotatewindow.h"
#include "projectcreator.h"
#include "layercreator.h"
#include "layerinformation.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Gimp Cutie");
    _clicked = false;
    _lastXPos = 0;
    _lastYPos = 0;
    ui->new_layer->setVisible(false);
    _fullscreen = false;

    //paramettrage de l'application
    _theme = 0;
    this->setStyleSheet("background-color: rgb(255,255,255);");

    //Création du menu
    setMenu();

    _imageSet = false;
    _crtlPressed = false;
    _saved = false;

    QPixmap mypix ("../yep_project2_2019/dimp_l.png");
    ui->logo_left->setPixmap(QPixmap(mypix));
    ui->logo_left->setVisible(true);

    //Définition des outils
    _tools.insert(std::make_pair("default", new defaultTool()));
    _tools.insert(std::make_pair("paintBrush", new PaintBrush()));
    _tools.insert(std::make_pair("pencil", new Pencil()));
    _tools.insert(std::make_pair("eraser", new Eraser()));
    _tools.insert(std::make_pair("colorPicker", new PickColor()));
    _tools.insert(std::make_pair("fillTool", new FillTool()));
    _toolName = "default";
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Fonction utilisant l'outil sélécrtionné
void MainWindow::draw_on_picture(int x, int y)
{
    int value = _selectedLayerName->objectName().split("_")[1].toInt();

    if (_layers[value].isLocked() || !_imageSet)return;
    _tools[_toolName]->useTool(
                _layers[value].getImage(),
                x,
                y,
                (value == 0) ? _currentProject.getColor() : Qt::transparent);
    _saved = false;
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName() + " *");
}

void MainWindow::update_picture()
{
    int value = _selectedLayerName->objectName().split("_")[1].toInt();

    _layers[value].getLabel()->setPixmap(QPixmap::fromImage(*_layers[value].getImage()));
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key::Key_Control)
        _crtlPressed = true;
    else if (_crtlPressed && e->key() == Qt::Key::Key_S)
        saveToJson();
}

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key::Key_Control)
        _crtlPressed = false;
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (_saved || !_imageSet) return;
    QMessageBox::StandardButton btn =
            QMessageBox::question(this,
                                  "",
                                  "You haven't saved your project !\nDo you want to save it ?",
                                  QMessageBox::Yes | QMessageBox::No);

    if (btn == QMessageBox::Yes)
        saveToJson();
    e->accept();
}

void MainWindow::createLayerRow(const QString &name, bool isLayer, bool isActif)
{
    QToolButton *eye_btn = new QToolButton(this);
    QToolButton *lock_btn = new QToolButton(this);
    MyQLabel *lb = new MyQLabel(this);
    int r = (ui->layer_information->count() / ui->layer_information->columnCount());

    //set eye button
    eye_btn->setIcon(QIcon("../icon/tools/eye.png"));
    eye_btn->setStyleSheet("border: 1px solid black");
    eye_btn->setCheckable(true);
    eye_btn->setObjectName(QString(std::string("eye_" + std::to_string(r)).c_str()));
    //set lock button
    lock_btn->setStyleSheet("border: 1px solid black; border-left: none");
    lock_btn->setCheckable(true);
    lock_btn->setObjectName(QString(std::string("lock_" + std::to_string(r)).c_str()));
    //set label
    lb->setStyleSheet("border: 1px solid black; border-left: none;");
    lb->setText(name);
    lb->setObjectName(QString(std::string("layer_" + std::to_string(r)).c_str()));

    //connect elements
    connect(eye_btn, SIGNAL(clicked()), this, SLOT(on_eye_clicked()));
    connect(lock_btn, SIGNAL(clicked()), this, SLOT(on_lock_clicked()));
    connect(lb, SIGNAL(clicked(QMouseEvent*)), this, SLOT(on_layer_clicked()));

    //setup elements on the screen
    ui->layer_information->addWidget(eye_btn, r, 0);
    ui->layer_information->addWidget(lock_btn, r, 1);
    ui->layer_information->addWidget(lb, r, 2);

    //setActif layer
    if (isActif && !isLayer) {
        lb->setStyleSheet("border: 1px solid black; border-left: none; background-color: gray");
        _selectedLayerName = lb;
    } else if (isActif && isLayer) {
        lb->setStyleSheet("border: 1px solid black; border-left: none; background-color: gray");
        _selectedLayerName->setStyleSheet("border: 1px solid black; border-left: none");
        _selectedLayerName = lb;
    }
    _saved = false;
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName() + " *");
}

void MainWindow::createLayer(Layer &layer, bool isNew)
{
    MyQLabel *l = new MyQLabel(this);
    QScrollArea *area = new QScrollArea(this);
    QGridLayout *g = new QGridLayout(this);

    if (isNew) {
        QImage *image = new QImage(_currentProject.getWidth(), _currentProject.getHeight(), QImage::Format_ARGB32);

        image->fill(layer.getFillColor());
        layer.setImage(image);
    }
    l->setMinimumSize(_currentProject.getWidth(), _currentProject.getHeight());
    l->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    l->setMouseTracking(true);
    l->setScaledContents(false);
    l->setPixmap(QPixmap::fromImage(*layer.getImage()));
    l->setStyleSheet("border: 1px solid black");

    connect(l, SIGNAL(clicked(QMouseEvent*)), this, SLOT(on_picture_clicked(QMouseEvent*)));
    connect(l, SIGNAL(release()), this, SLOT(on_picture_release()));
    connect(l, SIGNAL(moved(QMouseEvent*)), this, SLOT(on_picture_move(QMouseEvent*)));

    g->addWidget(l);
    area->setWidgetResizable(true);
    area->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    area->setStyleSheet("background-color: rgba(0, 0, 0, 0);");
    area->setLayout(g);
    layer.setScrollArea(area);
    layer.setLabel(l);
    ui->stack->addWidget(area, 0, 0);

    _layers.push_back(layer);
}

bool MainWindow::deleteLayer(int index)
{
    int firstWidget = 3 * (index);
    QLayoutItem *item;

    delete _layers[index].getImage();
    delete _layers[index].getLabel();
    delete _layers[index].getScrollArea();
    _layers.erase(_layers.begin() + index);
    for (int i = 0; i < 3; i++) {
        item = ui->layer_information->takeAt(firstWidget);
        delete item->widget();
        delete item;
    }
    QWidget *w;

    for (int i = 0; i < ui->layer_information->count() - firstWidget; i++) {
        w = ui->layer_information->itemAt(firstWidget + i)->widget();
        QString s = w->objectName().split("_")[0];
        int v = w->objectName().split("_")[1].toInt();

        w->setObjectName(QString(std::string(s.toStdString() + "_" + std::to_string(--v)).c_str()));
    }
    _selectedLayerName = qobject_cast<MyQLabel*>(ui->layer_information->itemAt(2)->widget());
    _selectedLayerName->setStyleSheet("border: 1px solid black; border-left: none; background-color: gray;");
    _saved = false;
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName() + " *");
    return true;
}

void MainWindow::clearProject()
{
    QLayoutItem* item;

    while ((item = ui->layer_information->takeAt(0)) != NULL) {
        delete item->widget();
        delete item;
    }
    for (Layer &l : _layers) {
        delete l.getImage();
        delete l.getLabel();
        delete l.getScrollArea();
    }
    _layers.clear();
}

void MainWindow::setMenu() {
    QAction *quit = new QAction("&Quit", this);
    quit->setIcon(QIcon("icons/tools/gimp-tool-heal.png"));
    QAction *clipboardImage = new QAction("&Importer depuis le presse papier", this);
    QAction *print = new QAction("&Imprimer", this);
    QAction *printPDF = new QAction("&Exporter au format PDF", this);
    QAction *resize = new QAction("&Redimensionner", this);
    QAction *rotate = new QAction("&Rotation", this);
    QAction *about = new QAction("&A propos", this);
    QAction *theme = new QAction("&Themes", this);
    QAction *createProject = new QAction("Créer un projet", this);
    QAction *openProject = new QAction("Ouvrir un projet", this);
    QAction *saveProject = new QAction("Sauvegarder", this);
    QAction *openImage = new QAction("Ouvrir une image", this);
    QAction *dowload = new QAction("Exporter l'image", this);
    QAction *addLayer = new QAction("Ajouter un calque", this);
    QAction *defaultTool = new QAction("Outil par défaut", this);
    QAction *eraserTool = new QAction("Gomme");
    QAction *paintTool = new QAction("Crayon", this);
    QAction *paintbrushTool = new QAction("Pinceau", this);
    QAction *colorPicker = new QAction("Pipette", this);
    QAction *back = new QAction("Annuler", this);
    QAction *restore = new QAction("Rétablir", this);
    QAction *cut = new QAction("Couper", this);
    QAction *copy = new QAction("Copier", this);
    QAction *paste = new QAction("Coller", this);
    QAction *erase = new QAction("Effacer", this);
    QAction *preference = new QAction("Préférence", this);
    QAction *all = new QAction("Tout", this);
    QAction *none = new QAction("Aucune", this);
    QAction *reverse = new QAction("Inverser", this);
    QAction *floating = new QAction("Flottante", this);
    QAction *bycolor = new QAction("Par couleur", this);
    QAction *fromthepath = new QAction("Depuis le chemin", this);
    QAction *selectioneditor = new QAction("Editeur de sélection", this);
    QAction *soften = new QAction("Adoucir", this);
    QAction *removesoftening = new QAction("Enlever l'adoucissement", this);
    QAction *reduce = new QAction("Réduire", this);
    QAction *enlarge = new QAction("Agrandir", this);
    QAction *border = new QAction("Bordure", this);
    QAction *deformation = new QAction("Déformation", this);
    QAction *rectanglearondi = new QAction("Rectangle arrondi", this);
    QAction *colorbalance = new QAction("Balance des couleurs", this);
    QAction *colortemperature = new QAction("Température de couleur", this);
    QAction *tintchroma = new QAction("Teinte-Chroma", this);
    QAction *tintsaturation = new QAction("Teinte-Saturation", this);
    QAction *saturation = new QAction("Saturation", this);
    QAction *exhibition = new QAction("Exposition", this);
    QAction *levels = new QAction("Niveaux", this);
    QAction *curves = new QAction("Courbes", this);
    QAction *threshold = new QAction("Seuil", this);
    QAction *coloring = new QAction("Colorier", this);
    QAction *posterize = new QAction("Postériser", this);
    QAction *colortowardsalpha = new QAction("Couleur vers alpha", this);
    QAction *Groupsofrecentlyclosedwindows = new QAction("Groupes de fenêtres récemment fermés", this);
    QAction *Toolbox = new QAction("Boite à outils", this);
    QAction *help = new QAction("Aide", this);
    QAction *Contextsensitivehelp = new QAction("Aide contextuelle", this);
    QAction *dailyadvice = new QAction("Conseil du jour", this);
    QAction *Searchandexecuteacommand = new QAction("Rechercher et exécuter une commande", this);
    QAction *pluginbrowser = new QAction("Navigateur de greffons", this);
    QAction *procedurebrowser = new QAction("Navigateur de procédures", this);
    QAction *GIMPOnline = new QAction("Gimp Cutie en ligne", this);
    QAction *Bugreportsandfeaturerequests = new QAction("Rapports de bogues et demandes de fonctionnalités", this);
    QAction *UsersManual = new QAction("Manuel de l'utilisateur", this);
    QAction *Duplicate = new QAction("Dupliquer", this);
    QAction *Canvassize = new QAction("Taille du canevas", this);
    QAction *Adjustthecanvastothelayers = new QAction("Ajuster le canevas aux calques", this);
    QAction *Adjustingthecanvastotheselection = new QAction("Ajuster le canevas à la sélection", this);
    QAction *Printsize = new QAction("Taille de l'impression", this);
    QAction *Scaleandimagesize = new QAction("Echelle et taille de l'image", this);
    QAction *Trimbyselection = new QAction("Rogner selon la sélection", this);
    QAction *Trimaccordingtocontent = new QAction("Rogner selon le contenu", this);
    QAction *SmartCutting = new QAction("Découpage futé", this);
    QAction *Cuttingusingtheguides = new QAction("Découper en utilisant les guides", this);
    QAction *Mergevisiblelayers = new QAction("Fusionner les calques visibles", this);
    QAction *Flattentheimage = new QAction("Aplatir l'image", this);
    QAction *Alignvisiblelayers = new QAction("Aligner les calques visibles", this);
    QAction *Configuringthegrid = new QAction("Configurer la grille", this);
    QAction *ImageProperties = new QAction("Propriétés de l'image", this);
    QAction *Newviews = new QAction("Nouvelles vue", this);
    QAction *Showall = new QAction("Tout afficher", this);
    QAction *Stitchforstitch = new QAction("Point pour point", this);
    QAction *Centertheimageinthewindow = new QAction("Centrer l'image dans la fenêtre", this);
    QAction *Adjustthewindowtotheimage = new QAction("Ajuster la fenêtre à l'image", this);
    QAction *Fullscreen = new QAction("Plein écran", this);
    QAction *Navigationwindow = new QAction("Fenêtre de navigation", this);
    QAction *Displayfilters = new QAction("Filtres d'affichage", this);
    QAction *Newfromthevisible = new QAction("Nouveau depuis le visible", this);
    QAction *Newlayergroup = new QAction("Nouveau groupe de calques", this);
    QAction *Duplicatethelayer = new QAction("Dupliquer le calque", this);
    QAction *Mergedown = new QAction("Fusionner vers le bas", this);
    QAction *Deletethelayer = new QAction("Supprimer le calque", this);
    QAction *Layeredgesize = new QAction("Taille des bords du calque", this);
    QAction *Layerwiththedimensionsoftheimage = new QAction("Calque aux dimensions de l'image", this);
    QAction *Layerscaleandsize = new QAction("Echelle et taille du calque", this);
    QAction *Trimbyselection2 = new QAction("Rogner selon la sélection", this);
    QAction *Trimbycontent = new QAction("Rogner selon le contenu", this);
    QAction *Paths = new QAction("Chemins", this);
    QAction *Text = new QAction("Texte", this);
    QAction *GEGLAction = new QAction("Action GEGL", this);
    QAction *Measure = new QAction("Mesure", this);
    QAction *Zoom2 = new QAction("Zoom", this);
    QAction *Toolbox2 = new QAction("Boite à outils", this);
    QAction *Defaultcolors = new QAction("Couleurs par défaut", this);
    QAction *Exchangingcolours = new QAction("Echanger les couleurs", this);
    QAction *Repeatthelast = new QAction("Répéter le dernier", this);
    QAction *Showlastoneagain = new QAction("Réafficher le dernier", this);
    QAction *Resetallfilters = new QAction("Réinitialiser tous les filtres", this);
    QAction *Blur = new QAction("Flou", this);
    QAction *Improvement = new QAction("Amélioration", this);
    QAction *Distortions = new QAction("Distortions", this);
    QAction *Shadowsandlights = new QAction("Ombres et lumières", this);
    QAction *Noise = new QAction("Bruit", this);
    QAction *Edgedetection = new QAction("Détection de bord", this);
    QAction *Generics = new QAction("Génériques", this);
    QAction *Combine = new QAction("Combiner", this);
    QAction *Artistic = new QAction("Artistiques", this);
    QAction *Decoration = new QAction("Décoration", this);
    QAction *Map = new QAction("Carte", this);
    QAction *Rendered = new QAction("Rendu", this);
    QAction *Web = new QAction("Web", this);
    QAction *Animation = new QAction("Animation", this);
    QAction *Alphatologo = new QAction("Alpha vers logo", this);
    QAction *Cancellationhistory = new QAction("Historique d'annulation", this);
    QAction *Copyvisible = new QAction("Copier visible", this);
    QAction *Pasteinplace = new QAction("Coller en place", this);
    QAction *Pasteintoselection = new QAction("Coller dans la sélection", this);
    QAction *Pasteintotheselectioninplace = new QAction("Coller dans la sélection en place", this);
    QAction *FillwiththecolourofPP = new QAction("Remplir avec la couleur de PP", this);
    QAction *FillwithAPcolour = new QAction("Remplir avec la couleur d'AP", this);
    QAction *Fillinwithapattern = new QAction("Remplir avec un motif", this);
    QAction *Fillinthecontouroftheselection = new QAction("Remplir le contour de la sélection", this);
    QAction *Fillingthepath = new QAction("Remplir le chemin", this);
    QAction *Tracetheselection = new QAction("Tracer la sélection", this);
    QAction *Leadingtheway = new QAction("Tracer le chemin", this);
    QAction *Inputdevices = new QAction("Périphériques d'entrée", this);
    QAction *Raccourcisclavier = new QAction("Raccourcis clavier", this);
    QAction *Modules = new QAction("Modules", this);
    QAction *Unites = new QAction("Unités", this);
    QAction *RGB = new QAction("RVB", this);
    QAction *Greylevel = new QAction("Niveau de gris", this);
    QAction *Indexedcolours = new QAction("Couleurs indexées", this);
    QAction *Integer8bits = new QAction("Entier 8 Bits", this);
    QAction *Integer16bits = new QAction("Entier 16 bits", this);
    QAction *Integer32bits = new QAction("Entier 32 bits", this);
    QAction *A16bitfloatingpoint = new QAction("Virgule flottante 16 bits", this);
    QAction *A32bitfloatingpoint = new QAction("Virgule flottante 32 bits", this);
    QAction *Perceptuelrange = new QAction("Gamma perceptuel", this);
    QAction *Linearlight = new QAction("Lumière linéraire", this);
    QAction *Enablecolormanagement = new QAction("Activer la gestion des couleurs", this);
    QAction *Assignacolorprofile = new QAction("Attribuer un profil de couleur", this);
    QAction *Converttocolorimetricprofile = new QAction("Convertir en profil colorimétrique", this);
    QAction *Rejectcolorprofile = new QAction("Rejeter le profil de couleur", this);
    QAction *Savingthecolorprofiletoafile = new QAction("Enregistrer le profil de couleur dans un fichier", this);
    QAction *Horizontalmirror = new QAction("Miroir horizontal", this);
    QAction *Verticalmirror = new QAction("Miroir vertical", this);
    QAction *A90clockwiserotation = new QAction("Rotation 90° sens horaire", this);
    QAction *A90counterclockwiserotation = new QAction("Rotation 90° sens anti-horaire", this);
    QAction *A180rotation = new QAction("Rotation 180°", this);
    QAction *Rotationarbitraire = new QAction("Rotation arbitraire", this);
    QAction *Removeallguides = new QAction("Enlever tous les guides", this);
    QAction *Newpercentageguide = new QAction("Nouveau guide en pourcentage", this);
    QAction *Newguide = new QAction("Nouveau guide", this);
    QAction *Newguidesfromtheselection = new QAction("Nouveaux guides depuis la sélection", this);
    QAction *Displaymetadata = new QAction("Afficher les métadonnées", this);
    QAction *Modifymetadata = new QAction("Modifier les métadonnées", this);
    QAction *Previouszoom = new QAction("Zoom précédent", this);
    QAction *Zoomout = new QAction("Zoom arrière", this);
    QAction *Zoomin = new QAction("Zoom avant", this);
    QAction *Adjustingtheimageinthewindow = new QAction("Ajuster l'image dans la fenêtre", this);
    QAction *Fittowindow = new QAction("Ajuster à la fenêtre", this);
    QAction *Zoomtoselection = new QAction("Zoomer jusqu'à la sélection", this);
    QAction *MirrorResetandRotation = new QAction("Réinitialisation Mirroir et rotation", this);
    QAction *Horizontalmirror2 = new QAction("Mirroir horizontal", this);
    QAction *Verticalmirror2 = new QAction("Mirroir vertical", this);
    QAction *A15clockwiserotation = new QAction("Rotation de 15° en sens horaire", this);
    QAction *A15counterclockwiserotation = new QAction("Rotation de 15° en sens anti-horaire", this);
    QAction *A90clockwiserotation2 = new QAction("Rotation de 90° en sens horaire", this);
    QAction *A90counterclockwiserotation2 = new QAction("Rotation de 90° en sens anti-horaire", this);
    QAction *A180rotation2 = new QAction("Rotation de 180°", this);
    QAction *Otherangleofrotation = new QAction("Autre angle de rotation", this);
    QAction *Colormanagementofthisview = new QAction("Gestion des couleurs de cette vue", this);
    QAction *Correctioncolours = new QAction("Couleurs de correction", this);
    QAction *Displayrenderingmode = new QAction("Mode de rendu de l'affichage", this);
    QAction *Blackpointcompensation = new QAction("Compensation du point noir", this);
    QAction *Onscreentestprofile = new QAction("Profil d'épreuve sur écran", this);
    QAction *Onscreenproofrenderingmode = new QAction("Mode de rendu de l'épreuve sur écran", this);
    QAction *Blackpointcompensation2 = new QAction("Compensation du point noir", this);
    QAction *Indicateoutofrangecolours = new QAction("Indiquer les couleurs hors gamme", this);
    QAction *Asinpreferences = new QAction("Comme dans les préférences", this);
    QAction *Rectangularselection = new QAction("Sélection rectangulaire", this);
    QAction *Ellipticalselection = new QAction("Sélection elliptique", this);
    QAction *Freehandselection = new QAction("Sélection à main levée", this);
    QAction *Extractionoftheforeground = new QAction("Extraction du premier plan", this);
    QAction *Contiguousselection = new QAction("Sélection contiguë", this);
    QAction *Selectionbycolor = new QAction("Sélection par couleur", this);
    QAction *Smartscissors = new QAction("Ciseaux intelligents", this);
    QAction *Filling = new QAction("Remplissage", this);
    QAction *Degraded = new QAction("Dégradé", this);
    QAction *Pencil = new QAction("Crayon", this);
    QAction *Brush = new QAction("Pinceau", this);
    QAction *Eraser = new QAction("Gomme", this);
    QAction *Airbrush = new QAction("Aérographe", this);
    QAction *Calligraphy = new QAction("Calligraphie", this);
    QAction *MyPaintBrush = new QAction("Brosse MyPaint", this);
    QAction *Cloning = new QAction("Clonage", this);
    QAction *Corrector = new QAction("Correcteur", this);
    QAction *PerspectiveCloning = new QAction("Clonage en perspective", this);
    QAction *BlurSharpness = new QAction("Flou / Netteté", this);
    QAction *Barbouiller = new QAction("Barbouiller", this);
    QAction *LightenDarken = new QAction("Eclaircir / Assombrir", this);
    QAction *Alignment = new QAction("Alignement", this);
    QAction *Travel = new QAction("Déplacement", this);
    QAction *Cutting = new QAction("Découpage", this);
    QAction *Rotation = new QAction("Rotation", this);
    QAction *Scalingup = new QAction("Mise à l'échelle", this);
    QAction *Shear = new QAction("Cisaillement", this);
    QAction *Perspective = new QAction("Perspective", this);
    QAction *Transformation3D = new QAction("Transformation 3D", this);
    QAction *UnifiedTransformation = new QAction("Transformation unifiée", this);
    QAction *Transformationbyhandles = new QAction("Transformation par poignées", this);
    QAction *Return = new QAction("Retourner", this);
    QAction *Transformationbycage = new QAction("Transformation par cage", this);
    QAction *Gauchir = new QAction("Gauchir", this);
    QAction *ToolOptions = new QAction("Options de l'outil", this);
    QAction *DeviceStatus = new QAction("Etat des périphériques", this);
    QAction *Layers = new QAction("Calques", this);
    QAction *Channels = new QAction("Canaux", this);
    QAction *Paths2 = new QAction("Chemins", this);
    QAction *IndexedColorPalette = new QAction("Palette des couleurs indexées", this);
    QAction *Histogram = new QAction("Histogramme", this);
    QAction *SelectionEditor = new QAction("Editeur de sélection", this);
    QAction *Navigation = new QAction("Navigation", this);
    QAction *Cancellationhistory2 = new QAction("Historique d'annulation", this);
    QAction *Pointer = new QAction("Pointeur", this);
    QAction *Samplingpoints = new QAction("Points d'échantillonnage", this);
    QAction *Symmetricalpainting = new QAction("Peinture en symétrie", this);
    QAction *Colours = new QAction("Couleurs", this);
    QAction *Brushes = new QAction("Brosses", this);
    QAction *BrushDynamics = new QAction("Dynamique de la brosse", this);
    QAction *MyPaintBrush2 = new QAction("Brosse MyPaint", this);
    QAction *Patterns = new QAction("Motifs", this);
    QAction *Degradations = new QAction("Dégradés", this);
    QAction *Pallets = new QAction("Palettes", this);
    QAction *Fonts = new QAction("Polices", this);
    QAction *Toolpresets = new QAction("Préréglages d'outils", this);
    QAction *Clipboard = new QAction("Presse-papiers", this);
    QAction *Images = new QAction("Images", this);
    QAction *DocumentHistory = new QAction("Historique des documments", this);
    QAction *Models = new QAction("Modèles", this);
    QAction *Errorconsole = new QAction("Console d'erreurs", this);
    QAction *Dashboard = new QAction("Tableau de bord", this);
    QAction *fullscreen = new QAction("Plein ecran", this);

    _menu = menuBar()->addMenu("&Fichier");
    _menu->addAction(createProject);
    _menu->addAction(openProject);
    _menu->addAction(saveProject);
    _menu->addAction(openImage);
    _menu->addAction(dowload);
    _menu->addAction(clipboardImage);
    _menu->addAction(print);
    _menu->addAction(printPDF);
    _menu->addSeparator();
    _menu->addAction(quit);
    _menu = menuBar()->addMenu("&Edition");
    _menu->addAction(back);
    _menu->addAction(restore);
    _menu->addAction(Cancellationhistory);
    _menu->addSeparator();
    _menu->addAction(cut);
    _menu->addAction(copy);
    _menu->addAction(Copyvisible);
    _menu->addAction(paste);
    _menu->addAction(Pasteinplace);
    _menu->addAction(Pasteintoselection);
    _menu->addAction(Pasteintotheselectioninplace);
    _menu->addSeparator();
    _menu->addAction(erase);
    _menu->addAction(FillwiththecolourofPP);
    _menu->addAction(FillwithAPcolour);
    _menu->addAction(Fillinwithapattern);
    _menu->addAction(Fillinthecontouroftheselection);
    _menu->addAction(Fillingthepath);
    _menu->addAction(Tracetheselection);
    _menu->addAction(Leadingtheway);
    _menu->addSeparator();
    _menu->addAction(preference);
    _menu->addAction(Inputdevices);
    _menu->addAction(Raccourcisclavier);
    _menu->addAction(Modules);
    _menu->addAction(Unites);
    _menu = menuBar()->addMenu("&Sélection");
    _menu->addAction(all);
    _menu->addAction(none);
    _menu->addAction(reverse);
    _menu->addAction(floating);
    _menu->addAction(bycolor);
    _menu->addAction(fromthepath);
    _menu->addAction(selectioneditor);
    _menu->addSeparator();
    _menu->addAction(soften);
    _menu->addAction(removesoftening);
    _menu->addAction(reduce);
    _menu->addAction(enlarge);
    _menu->addAction(border);
    _menu->addAction(deformation);
    _menu->addAction(rectanglearondi);
    _menu = menuBar()->addMenu("&Affichage");
    _menu->addAction(Newviews);
    _menu->addAction(Showall);
    _menu->addAction(Stitchforstitch);
    _menuZoom = _menu->addMenu(tr("&Zoom"));
    _menuZoom->addAction(Previouszoom);
    _menuZoom->addAction(Zoomout);
    _menuZoom->addAction(Zoomin);
    _menuZoom->addAction(Adjustingtheimageinthewindow);
    _menuZoom->addAction(Fittowindow);
    _menuZoom->addAction(Zoomtoselection);
    _menuMR = _menu->addMenu(tr("&Mirroir et rotation"));
    _menuMR->addAction(MirrorResetandRotation);
    _menuMR->addSeparator();
    _menuMR->addAction(Horizontalmirror2);
    _menuMR->addAction(Verticalmirror2);
    _menuMR->addSeparator();
    _menuMR->addAction(A15clockwiserotation);
    _menuMR->addAction(A15counterclockwiserotation);
    _menuMR->addAction(A90clockwiserotation2);
    _menuMR->addAction(A90counterclockwiserotation2);
    _menuMR->addAction(A180rotation2);
    _menuMR->addSeparator();
    _menuMR->addAction(Otherangleofrotation);
    _menu->addAction(Centertheimageinthewindow);
    _menu->addSeparator();
    _menu->addAction(Adjustthewindowtotheimage);
    _menu->addAction(Fullscreen);
    _menu->addSeparator();
    _menu->addAction(Navigationwindow);
    _menu->addAction(Displayfilters);
    _menuCM = _menu->addMenu(tr("&Gestion des couleurs"));
    _menuCM->addAction(Colormanagementofthisview);
    _menuCM->addAction(Correctioncolours);
    _menuCM->addSeparator();
    _menuCM->addAction(Displayrenderingmode);
    _menuCM->addAction(Blackpointcompensation);
    _menuCM->addSeparator();
    _menuCM->addAction(Onscreentestprofile);
    _menuCM->addAction(Onscreenproofrenderingmode);
    _menuCM->addAction(Blackpointcompensation2);
    _menuCM->addAction(Indicateoutofrangecolours);
    _menuCM->addSeparator();
    _menuCM->addAction(Asinpreferences);
    _menu->addAction(fullscreen);
    _menu = menuBar()->addMenu("&Image");
    _menu->addAction(resize);
    _menu->addAction(rotate);
    _menu->addSeparator();
    _menu->addAction(Duplicate);
    _menu->addSeparator();
    _menuMode = _menu->addMenu(tr("&Mode"));
    _menuMode->addAction(RGB);
    _menuMode->addAction(Greylevel);
    _menuMode->addAction(Indexedcolours);
    _menuAccu = _menu->addMenu(tr("&Précision"));
    _menuAccu->addAction(Integer8bits);
    _menuAccu->addAction(Integer16bits);
    _menuAccu->addAction(Integer32bits);
    _menuAccu->addAction(A16bitfloatingpoint);
    _menuAccu->addAction(A32bitfloatingpoint);
    _menuAccu->addSeparator();
    _menuAccu->addAction(Perceptuelrange);
    _menuAccu->addAction(Linearlight);
    _menuCM2 = _menu->addMenu(tr("&Gestion des couleurs"));
    _menuCM2->addAction(Enablecolormanagement);
    _menuCM2->addSeparator();
    _menuCM2->addAction(Assignacolorprofile);
    _menuCM2->addAction(Converttocolorimetricprofile);
    _menuCM2->addAction(Rejectcolorprofile);
    _menuCM2->addSeparator();
    _menuCM2->addAction(Savingthecolorprofiletoafile);
    _menu->addSeparator();
    _menuTransform = _menu->addMenu(tr("&Transformer"));
    _menuTransform->addAction(Horizontalmirror);
    _menuTransform->addAction(Verticalmirror);
    _menuTransform->addAction(A90clockwiserotation);
    _menuTransform->addAction(A90counterclockwiserotation);
    _menuTransform->addAction(A180rotation);
    _menuTransform->addAction(Rotationarbitraire);
    _menu->addAction(Canvassize);
    _menu->addAction(Adjustthecanvastothelayers);
    _menu->addAction(Adjustingthecanvastotheselection);
    _menu->addAction(Printsize);
    _menu->addAction(Scaleandimagesize);
    _menu->addSeparator();
    _menu->addAction(Trimbyselection);
    _menu->addAction(Trimaccordingtocontent);
    _menu->addAction(SmartCutting);
    _menu->addAction(Cuttingusingtheguides);
    _menu->addSeparator();
    _menu->addAction(Mergevisiblelayers);
    _menu->addAction(Flattentheimage);
    _menu->addAction(Alignvisiblelayers);
    _menu->addSeparator();
    _menuGuide = _menu->addMenu(tr("&Guides"));
    _menuGuide->addAction(Removeallguides);
    _menuGuide->addAction(Newpercentageguide);
    _menuGuide->addAction(Newguide);
    _menuGuide->addAction(Newguidesfromtheselection);
    _menu->addAction(Configuringthegrid);
    _menu->addSeparator();
    _menu->addAction(ImageProperties);
    _menuMeta = _menu->addMenu(tr("&Métadonnées"));
    _menuMeta->addAction(Displaymetadata);
    _menuMeta->addAction(Modifymetadata);
    _menu = menuBar()->addMenu("&Calque");
    _menu->addAction(addLayer);
    _menu->addAction(Newfromthevisible);
    _menu->addAction(Newlayergroup);
    _menu->addAction(Duplicatethelayer);
    _menu->addAction(Mergedown);
    _menu->addAction(Deletethelayer);
    _menu->addSeparator();
    _menu->addAction(Layeredgesize);
    _menu->addAction(Layerwiththedimensionsoftheimage);
    _menu->addAction(Layerscaleandsize);
    _menu->addAction(Trimbyselection2);
    _menu->addAction(Trimbycontent);
    _menu = menuBar()->addMenu("&Couleurs");
    _menu->addAction(colorbalance);
    _menu->addAction(colortemperature);
    _menu->addAction(tintchroma);
    _menu->addAction(tintsaturation);
    _menu->addAction(saturation);
    _menu->addAction(exhibition);
    _menu->addAction(levels);
    _menu->addAction(curves);
    _menu->addSeparator();
    _menu->addAction(threshold);
    _menu->addAction(coloring);
    _menu->addAction(posterize);
    _menu->addAction(colortowardsalpha);
    _menu = menuBar()->addMenu("&Outils");
    _menu->addAction(defaultTool);
    _menu->addAction(eraserTool);
    _menu->addAction(paintTool);
    _menu->addAction(paintbrushTool);
    _menu->addAction(colorPicker);
    _menuOS = _menu->addMenu(tr("&Outils de sélection"));
    _menuOS->addAction(Rectangularselection);
    _menuOS->addAction(Ellipticalselection);
    _menuOS->addAction(Freehandselection);
    _menuOS->addAction(Extractionoftheforeground);
    _menuOS->addAction(Contiguousselection);
    _menuOS->addAction(Selectionbycolor);
    _menuOS->addAction(Smartscissors);
    _menuOS = _menu->addMenu(tr("&Outils de peinture"));
    _menuOS->addAction(Filling);
    _menuOS->addAction(Degraded);
    _menuOS->addAction(Pencil);
    _menuOS->addAction(Brush);
    _menuOS->addAction(Eraser);
    _menuOS->addAction(Airbrush);
    _menuOS->addAction(Calligraphy);
    _menuOS->addAction(MyPaintBrush);
    _menuOS->addAction(Cloning);
    _menuOS->addAction(Corrector);
    _menuOS->addAction(PerspectiveCloning);
    _menuOS->addAction(BlurSharpness);
    _menuOS->addAction(Barbouiller);
    _menuOS->addAction(LightenDarken);
    _menuOT = _menu->addMenu(tr("&Outils de transformation"));
    _menuOT->addAction(Alignment);
    _menuOT->addAction(Travel);
    _menuOT->addAction(Cutting);
    _menuOT->addAction(Rotation);
    _menuOT->addAction(Scalingup);
    _menuOT->addAction(Shear);
    _menuOT->addAction(Perspective);
    _menuOT->addAction(Transformation3D);
    _menuOT->addAction(UnifiedTransformation);
    _menuOT->addAction(Transformationbyhandles);
    _menuOT->addAction(Return);
    _menuOT->addAction(Transformationbycage);
    _menuOT->addAction(Gauchir);
    _menu->addAction(Paths);
    _menu->addAction(Text);
    _menu->addAction(GEGLAction);
    _menu->addSeparator();
    _menu->addAction(Measure);
    _menu->addAction(Zoom2);
    _menu->addSeparator();
    _menu->addAction(Toolbox2);
    _menu->addAction(Defaultcolors);
    _menu->addAction(Exchangingcolours);
    _menu = menuBar()->addMenu("&Filtres");
    _menu->addAction(Repeatthelast);
    _menu->addAction(Showlastoneagain);
    _menu->addAction(Resetallfilters);
    _menu->addSeparator();
    _menu->addAction(Blur);
    _menu->addAction(Improvement);
    _menu->addAction(Distortions);
    _menu->addAction(Shadowsandlights);
    _menu->addAction(Noise);
    _menu->addAction(Edgedetection);
    _menu->addAction(Generics);
    _menu->addAction(Combine);
    _menu->addAction(Artistic);
    _menu->addAction(Decoration);
    _menu->addAction(Map);
    _menu->addAction(Rendered);
    _menu->addAction(Web);
    _menu->addAction(Animation);
    _menu->addSeparator();
    _menu->addAction(Alphatologo);
    _menu = menuBar()->addMenu("&Fenêtres");
    _menu->addAction(Groupsofrecentlyclosedwindows);
    _menuFA = _menu->addMenu(tr("&Fenêtre ancrables"));
    _menuFA->addAction(ToolOptions);
    _menuFA->addAction(DeviceStatus);
    _menuFA->addSeparator();
    _menuFA->addAction(Layers);
    _menuFA->addAction(Channels);
    _menuFA->addAction(Paths2);
    _menuFA->addAction(IndexedColorPalette);
    _menuFA->addAction(Histogram);
    _menuFA->addAction(SelectionEditor);
    _menuFA->addAction(Navigation);
    _menuFA->addAction(Cancellationhistory2);
    _menuFA->addAction(Pointer);
    _menuFA->addAction(Samplingpoints);
    _menuFA->addAction(Symmetricalpainting);
    _menuFA->addSeparator();
    _menuFA->addAction(Colours);
    _menuFA->addAction(Brushes);
    _menuFA->addAction(BrushDynamics);
    _menuFA->addAction(MyPaintBrush2);
    _menuFA->addAction(Patterns);
    _menuFA->addAction(Degradations);
    _menuFA->addAction(Pallets);
    _menuFA->addAction(Fonts);
    _menuFA->addAction(Toolpresets);
    _menuFA->addAction(Clipboard);
    _menuFA->addSeparator();
    _menuFA->addAction(Images);
    _menuFA->addAction(DocumentHistory);
    _menuFA->addAction(Models);
    _menuFA->addAction(Errorconsole);
    _menuFA->addAction(Dashboard);
    _menu->addAction(Toolbox);
    _menu = menuBar()->addMenu("&Aide");
    _menu->addAction(help);
    _menu->addAction(Contextsensitivehelp);
    _menu->addAction(dailyadvice);
    _menu->addAction(about);
    _menu->addSeparator();
    _menu->addAction(Searchandexecuteacommand);
    _menu->addSeparator();
    _menu->addAction(pluginbrowser);
    _menu->addAction(procedurebrowser);
    _menu->addSeparator();
    _menu->addAction(GIMPOnline);
    _menu->addAction(Bugreportsandfeaturerequests);
    _menu->addAction(UsersManual);
    _menu->addAction(theme);

    connect(createProject, SIGNAL(triggered()), this, SLOT(on_create_project_clicked()));
    connect(openProject, SIGNAL(triggered()), this, SLOT(on_open_project_clicked()));
    connect(saveProject, SIGNAL(triggered()), this, SLOT(on_save_project_clicked()));
    connect(openImage, SIGNAL(triggered()), this, SLOT(on_pushButton_clicked()));
    connect(dowload, SIGNAL(triggered()), this, SLOT(on_export_btn_clicked()));
    connect(quit, &QAction::triggered, this, QApplication::quit);
    connect(clipboardImage, SIGNAL(triggered()), this, SLOT(clipboardPasteImage()));
    connect(print, SIGNAL(triggered()), this, SLOT(printImage()));
    connect(printPDF, SIGNAL(triggered()), this, SLOT(printImagePDF()));
    connect(resize, SIGNAL(triggered()), this, SLOT(resize()));
    connect(rotate, SIGNAL(triggered()), this, SLOT(rotate()));
    connect(about, SIGNAL(triggered()), this, SLOT(menuAbout()));
    connect(theme, SIGNAL(triggered()), this, SLOT(menuTheme()));
    connect(addLayer, SIGNAL(triggered()), this, SLOT(on_new_layer_clicked()));
    connect(defaultTool, SIGNAL(triggered()), this, SLOT(on_default_tool_clicked()));
    connect(eraserTool, SIGNAL(triggered()), this, SLOT(on_eraser_tool_clicked()));
    connect(paintTool, SIGNAL(triggered()), this, SLOT(on_pencil_tool_clicked()));
    connect(paintbrushTool, SIGNAL(triggered()), this, SLOT(on_paintbrush_tool_clicked()));
    connect(colorPicker, SIGNAL(triggered()), this, SLOT(on_pickcolor_tool_clicked()));
    connect(fullscreen, SIGNAL(triggered()), this, SLOT(fullscreen()));
}

void MainWindow::menuAbout() {
    menuAboutWindow window;
    window.setModal(true);
    window.exec();
}

void MainWindow::menuTheme() {
    menuThemeWindow window;
    window.setModal(true);
    window.setTheme(_theme);
    window.exec();
    int theme = window.getTheme();
    if (theme != _theme) {
        _theme = theme;
        if (_theme == 0) {
            this->setStyleSheet("background-color: rgb(255,255,255);");
        } else {
            this->setStyleSheet("background-color: rgb(171,171,171);");
        }
    }
}

//Fonction pour ouvrir une image
void MainWindow::on_pushButton_clicked()
{
    QString file_path = QFileDialog::getOpenFileName(this, "Open image", ".", "Images (*.png *.xpm *.jpg)");
    if (file_path.isEmpty()) return;

    QImage file = QImage(file_path);
    QStringList list = file_path.split('/');

    if (!_imageSet) {
        _currentProject.setWidth(file.width());
        _currentProject.setHeight(file.height());
        _currentProject.setTransparent(true);
        _currentProject.setColor(QColor(Qt::transparent));
        _currentProject.setName(list.last().split(".")[0]);
        this->setWindowTitle("Gimp Cutie - " + _currentProject.getName() + " *");

        ui->new_layer->setVisible(true);
    }

    QImage *image = new QImage(_currentProject.getWidth(), _currentProject.getHeight(), QImage::Format_ARGB32);
    QPainter painter(image);
    Layer layer;

    image->fill(Qt::transparent);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(image->width() / 2 - file.width() / 2, image->height() / 2 - file.height() / 2, file);
    layer.setImage(image);
    layer.setName(list.last());
    _saved = false;

    createLayer(layer, false);
    createLayerRow(layer.getName(), _imageSet, true);
    _imageSet = true;
}

//Détection du clique au sein de l'image
void MainWindow::on_picture_clicked(QMouseEvent *even)
{
    if (!_clicked) {
        //Utilisation d'outil à la place de cette fonction
        _lastXPos = even->x();
        _lastYPos = even->y();
        draw_on_picture(_lastXPos, _lastYPos);
        update_picture();
        _clicked = true;
    }
}

//Détéction fin de clique
void MainWindow::on_picture_release()
{
    _clicked = false;
}

//Détéction du mouvement de la souris si le bouton a été cliqué précédement
void MainWindow::on_picture_move(QMouseEvent *even)
{
    if (_clicked) {
        int newXPos = even->x();
        int newYPos = even->y();
        int xDirection = (_lastXPos - newXPos < 0) ? -1 : 1;
        int yDirection = (_lastYPos - newYPos < 0) ? -1 : 1;
        int x = (_lastXPos - newXPos) * xDirection;
        int y = (_lastYPos - newYPos) * yDirection;
        double distance = sqrt(pow(x, 2) + pow(y, 2));
        for (int dist = 1; dist < distance; dist++) {
            draw_on_picture(dist * x / distance * xDirection + _lastXPos, dist * y / distance * yDirection + _lastYPos);
        }
        update_picture();
        _lastXPos = newXPos;
        _lastYPos = newYPos;
    }
}

void MainWindow::on_paintbrush_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "paintBrush";
    this->uncheckButtons();
    ui->paintbrush_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    QPixmap ci = QPixmap("://icons/tools/gimp-tool-paintbrush.png");
    QCursor cursor(ci);
    this->setCursor(cursor);
}

void MainWindow::on_default_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "default";
    this->uncheckButtons();
    ui->default_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    this->setCursor(Qt::ArrowCursor);
}

void MainWindow::uncheckButtons()
{
    ui->default_tool->setChecked(false);
    ui->paintbrush_tool->setChecked(false);
    ui->pencil_tool->setChecked(false);
    ui->eraser_tool->setChecked(false);
    ui->pickcolor_tool->setChecked(false);
    ui->fill_tool->setChecked(false);
}

void MainWindow::saveToJson()
{
    //Création du dossiser regroupant les projects.
    QDir dir;
    QString path = "./projects/";
    if (!dir.exists(path))
        dir.mkpath(path);

    //Création du fichier JSON du nouveau project
    QFile saveFile(QString(path + _currentProject.getName() + ".json"));
    if (!saveFile.open(QIODevice::WriteOnly))
        std::cout << "Error while create saving file" << std::endl;

    //Mise en place des valeurs du project
    QJsonObject json;
    _currentProject.writeInJSon(json);

    //Sauvegarde des layers
    QJsonArray array;
    for (Layer l : _layers) {
        QJsonObject v;

        v["name"] = l.getName();
        QBuffer buffer;
        buffer.open(QIODevice::WriteOnly);
        l.getImage()->save(&buffer, "PNG");
        auto const encoded = buffer.data().toBase64();
        v["image"] = QJsonValue(QLatin1String(encoded));
        array.push_back(v);
    }
    json["layers"] = array;

    //Enregistrement du project
    QJsonDocument doc(json);
    saveFile.write(doc.toJson());
    _saved = true;
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName());
}

void MainWindow::on_pencil_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "pencil";
    this->uncheckButtons();
    ui->pencil_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    QPixmap ci = QPixmap("://icons/tools/gimp-tool-pencil.png");
    QCursor cursor(ci);
    this->setCursor(cursor);
}

void MainWindow::on_eraser_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "eraser";
    this->uncheckButtons();
    ui->eraser_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    QPixmap ci = QPixmap("://icons/tools/gimp-tool-eraser.png");
    QCursor cursor(ci);
    this->setCursor(cursor);
}

void MainWindow::on_pickcolor_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "colorPicker";
    this->uncheckButtons();
    ui->pickcolor_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    QPixmap ci = QPixmap("://icons/tools/gimp-tool-color-picker.png");
    QCursor cursor(ci);
    this->setCursor(cursor);
}

void MainWindow::on_create_project_clicked()
{
    // Affichage de la popup
    ProjectCreator creator;
    creator.setModal(true);
    creator.exec();
    if (creator.cancelled())
        return;

    //Delete previsous project
    if (_imageSet)
        clearProject();
    //Récupération des informations du nouveau projet
    _currentProject = creator.getProject();

    //Création et affichage de l'image souhaitée.
    QImage *image = new QImage(_currentProject.getWidth(), _currentProject.getHeight(), QImage::Format_ARGB32);
    Layer layer;

    image->fill(_currentProject.getColor());
    _imageSet = true;
    layer.setImage(image);
    layer.setName(_currentProject.getName());
    createLayer(layer, false);

    //Add layers informations
    createLayerRow(_currentProject.getName(), false, true);
    ui->new_layer->setVisible(true);

    _saved = false;
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName() + " *");
}

void MainWindow::on_open_project_clicked()
{
    //ask for save
    if (!_saved && _imageSet) {
        QMessageBox::StandardButton btn = QMessageBox::question(this, "",
                                                                "You haven't saved your project !\nDo you want to save it ?",
                                                                QMessageBox::Yes | QMessageBox::No);
        if (btn == QMessageBox::Yes)
            saveToJson();
    }
    //open json project
    QString file = QFileDialog::getOpenFileName(this, "Open project file", "./projects", "Files (*.json)");
    QFile saveFile(file);
    if (!saveFile.open(QIODevice::ReadOnly)) {
        std::cout << "Error while openning project file" << std::endl;
        return;
    }

    //delete previsous project
    if (_imageSet)
        clearProject();

    //load data from json
    QByteArray saveData = saveFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject json = loadDoc.object();

    //get Project data from json
    _currentProject.setName(json["name"].toString());
    _currentProject.setWidth(json["width"].toInt());
    _currentProject.setHeight(json["height"].toInt());

    //get Project Color from json
    QJsonObject bg = json["background"].toObject();
    QColor color(bg["red"].toInt(), bg["green"].toInt(), bg["blue"].toInt(), bg["alpha"].toInt());
    _currentProject.setColor(color);
    _currentProject.setTransparent(json["transparency"].toBool());

    _imageSet = true;
    _saved = true;
    ui->new_layer->setVisible(true);

    //load layers from json
    QJsonArray array = json["layers"].toArray();
    int i =  0;

    _selectedLayerName = new MyQLabel(this);
    for (const QJsonValue v : array) {
        Layer layer;
        QImage *image = new QImage();

        image->loadFromData(QByteArray::fromBase64(v["image"].toString().toLatin1()), "PNG");
        layer.setTransparent(true);
        layer.setName(v["name"].toString());
        layer.setImage(image);
        createLayer(layer, false);
        createLayerRow(layer.getName(), true, (i == 0));
        i++;
    }
    this->setWindowTitle("Gimp Cutie - " + _currentProject.getName());
}

void MainWindow::on_save_project_clicked()
{
    if (!_imageSet) return;
    saveToJson();
}

void MainWindow::printImage()
{
    QPrinter printer;
    QPrintDialog printDialog(&printer);
    if (printDialog.exec() == QDialog::Accepted) {
        printImagePDF();
    }
}

void MainWindow::printImagePDF()
{
    QString fileName;
    if ((fileName = QFileDialog::getSaveFileName(0, "Export PDF",QString(), "*.pdf")) == nullptr) return;

    QPrinter printer;
    QPainter painter;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(fileName);
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return ;
    }
    QImage source(_currentProject.getWidth(), _currentProject.getHeight(), QImage::Format_ARGB32);
    QPainter drawer(&source);

    if (fileName.isEmpty()) return;
    drawer.setCompositionMode(QPainter::CompositionMode_SourceOver);
    for (Layer &l : _layers)
        drawer.drawImage(0, 0, *l.getImage());
    painter.drawImage(source.rect(), source);
    painter.end();
}

void MainWindow::resize()
{
    menuResizeWindow window(_currentProject.getWidth(), _currentProject.getHeight());
    window.setModal(true);
    window.exec();
    QSize size = window.getSize();
    if (size.width() <= 0 || size.height() <= 0) return ;

    _currentProject.setWidth(size.width());
    _currentProject.setHeight(size.height());
    for (Layer &l : _layers) {
        *l.getImage() = l.getImage()->scaled(size);
        l.getLabel()->setMinimumSize(l.getImage()->width(), l.getImage()->height());
        l.getLabel()->resize(l.getImage()->width(), l.getImage()->height());
        l.getLabel()->setPixmap(QPixmap::fromImage(*l.getImage()));
    }
    _saved = false;
}

void MainWindow::rotate()
{
    menuRotateWindow window;
    window.setModal(true);
    window.exec();
    QTransform rotation;
    int angle = window.getAngle();
    rotation.rotate(angle);
    for (Layer &l : _layers) {
        *l.getImage() = l.getImage()->transformed(rotation);
        l.getLabel()->setMinimumSize(l.getImage()->width(), l.getImage()->height());
        l.getLabel()->resize(l.getImage()->width(), l.getImage()->height());
        l.getLabel()->setPixmap(QPixmap::fromImage(*l.getImage()));
    }
    _saved = false;
}

void MainWindow::clipboardPasteImage()
{
    QClipboard *pressePapiers = QApplication::clipboard();
    QImage *img = new QImage(pressePapiers->image());

    if (img == nullptr) return;

    Layer layer;
    layer.setImage(img);
    layer.setName(pressePapiers->text());
    createLayer(layer, false);
    createLayerRow(pressePapiers->text(), true, true);
}

void MainWindow::on_eye_clicked()
{
    QToolButton *btn = qobject_cast<QToolButton*>(sender());
    int value = btn->objectName().split('_')[1].toInt();

    btn->setIcon((btn->isChecked() ? QIcon() : QIcon("../icon/tools/eye.png")));
    _layers[value].getScrollArea()->setVisible(!btn->isChecked());
}

void MainWindow::on_lock_clicked()
{
    QToolButton *btn = qobject_cast<QToolButton*>(sender());
    int value = btn->objectName().split('_')[1].toInt();

    btn->setIcon((btn->isChecked() ? QIcon("../icon/tools/lock.png") : QIcon()));
    _layers[value].setLocked(btn->isChecked());
}

void MainWindow::on_new_layer_clicked()
{
    if (!_imageSet) return;
    LayerCreator creator;
    creator.setModal(true);
    creator.exec();

    Layer layer = creator.getLayer();
    createLayer(layer);
    createLayerRow(layer.getName(), true, true);
}

void MainWindow::on_layer_clicked()
{
    MyQLabel *lb = qobject_cast<MyQLabel*>(sender());
    int value = lb->objectName().split("_")[1].toInt();

    if (_selectedLayerName == lb) {
        LayerInformation info(_layers[value]);
        info.setModal(true);
        info.exec();
        if (!info.toDelete()) return;
        (info.toDelete() && _layers.size() > 1) ?
                    deleteLayer(value) :
                    QMessageBox::information(this, "Alert", "Your project must contain at least one layer");
    } else {
        _selectedLayerName->setStyleSheet("border: 1px solid black; border-left: none");
        lb->setStyleSheet("border: 1px solid black; border-left: none; background-color: gray;");
        _selectedLayerName = lb;
    }
}

void MainWindow::on_export_btn_clicked()
{
    if (!_imageSet) return;
    QImage source(_currentProject.getWidth(), _currentProject.getHeight(), QImage::Format_ARGB32);
    QPainter painter(&source);
    QString fileName =
            QFileDialog::getSaveFileName(this,
                                         tr("Save Image File"),
                                         QString(),
                                         tr("PNG (*.png);;JPG (*.jpg);;JPEG (*.jpeg);;BMP (*.bmp)"));

    if (fileName.isEmpty()) return;
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    for (Layer &l : _layers)
        painter.drawImage(0, 0, *l.getImage());
    source.save(fileName);
  }

void MainWindow::fullscreen()
{
    if (_fullscreen) {
        showNormal();
        _fullscreen = false;
    } else {
        showFullScreen();
        _fullscreen = true;
    }
}

void MainWindow::on_fill_tool_clicked()
{
    QColor color = _tools[_toolName]->getColor();
    _toolName = "fillTool";
    this->uncheckButtons();
    ui->fill_tool->setChecked(true);
    _tools[_toolName]->setColor(color);
    _tools[_toolName]->getToolBox(ui->tool_information);
    QPixmap ci = QPixmap("://icons/tools/gimp-tool-bucket-fill.png");
    QCursor cursor(ci);
    this->setCursor(cursor);
}

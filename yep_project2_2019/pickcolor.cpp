#include "pickcolor.h"

PickColor::PickColor()
{
}

PickColor::~PickColor()
{
}

void PickColor::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    (void)colorLayer;
    _color = image->pixelColor(x, y);
    _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(_color.red()) + ", " + std::to_string(_color.green()) + ", " + std::to_string(_color.blue()) + ", " + std::to_string(_color.alpha()) + ")").c_str());
}

void PickColor::getToolBox(QFormLayout *layout)
{
    _boxColor = new QPushButton();

    removeOldToolBox(layout);

    _boxColor->setText("");
    _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(_color.red()) +
                                         ", " + std::to_string(_color.green()) +
                                         ", " + std::to_string(_color.blue()) +
                                         ", " + std::to_string(_color.alpha()) + ")").c_str());

    connect(_boxColor, SIGNAL(clicked()), this, SLOT(on_color_clicked()));

    layout->addRow("Couleur", _boxColor);
}

void PickColor::on_color_clicked()
{
    QColor c = QColorDialog::getColor(_color, this, "Choose a color");

    if (c.isValid()) {
        _color = c;
        _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(c.red()) + ", " + std::to_string(c.green()) + ", " + std::to_string(c.blue()) + ", " + std::to_string(c.alpha()) + ")").c_str());
    }
}

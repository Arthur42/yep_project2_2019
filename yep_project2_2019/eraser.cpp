#include "eraser.h"
#include "ui_mainwindow.h"

Eraser::Eraser()
{
    _square = false;
}

void Eraser::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    if (_size <= 0) return;
    int minX = (x - _size / 2 > 0) ? x - _size / 2 : 0;
    int minY = (y - _size / 2 > 0) ? y - _size / 2 : 0;
    (_square) ? erase_square(image, minX, minY, colorLayer) : erase_circle(image, minX, minY, colorLayer);
}

void Eraser::getToolBox(QFormLayout *layout)
{
    _boxSize = new QLineEdit();
    _boxCheck = new QCheckBox();

     removeOldToolBox(layout);

    _boxSize->setValidator(new QIntValidator);
    _boxSize->setText(QString(std::to_string(_size).c_str()));
    _boxCheck->setText("");
    _boxCheck->setCheckState((_square) ? Qt::Checked : Qt::Unchecked);
    _boxCheck->setStyleSheet("QCheckBox::indicator { width:25px; height: 25px;}");
    connect(_boxSize, SIGNAL(returnPressed()), this, SLOT(on_size_editingfinished()));
    connect(_boxCheck, SIGNAL(stateChanged(int)), this, SLOT(on_check_stateChange(int)));
    layout->addRow("Taille", _boxSize);
    layout->addRow("Forme carré", _boxCheck);
}

void Eraser::on_size_editingfinished()
{
    QString str = _boxSize->text();
    int value = str.toInt();

    _boxSize->clearFocus();
    _size = value;
}

void Eraser::on_check_stateChange(int status)
{
    _square = (status == 0) ? false : true;
}

void Eraser::erase_circle(QImage *image, int x, int y, const QColor &colorLayer)
{
    QPainter painter(image);
    QPen pen;

    painter.setCompositionMode(QPainter::CompositionMode_Source);
    pen.setStyle(Qt::NoPen);
    painter.setPen(pen);
    painter.setBrush(QBrush(colorLayer));
    painter.drawEllipse(x, y, _size, _size);
}

void Eraser::erase_square(QImage *image, int x, int y, const QColor &colorLayer)
{
    QPainter painter(image);

    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(x, y, _size, _size, colorLayer);
}

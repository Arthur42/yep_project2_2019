#include "menuthemewindow.h"
#include "ui_menuthemewindow.h"
#include <iostream>

menuThemeWindow::menuThemeWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuThemeWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Themes");
    _theme = 0;
}

menuThemeWindow::~menuThemeWindow()
{
    delete ui;
}

void menuThemeWindow::setTheme(int theme)
{
    _theme = theme;
    if (theme == 0) {
        ui->checkBox->setCheckState(Qt::CheckState::Checked);
        ui->checkBox_2->setCheckState(Qt::CheckState::Unchecked);
    } else {
        ui->checkBox->setCheckState(Qt::CheckState::Unchecked);
        ui->checkBox_2->setCheckState(Qt::CheckState::Checked);
    }
}

int menuThemeWindow::getTheme()
{
    return _theme;
}

void menuThemeWindow::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == Qt::CheckState::Checked) {
        _theme = 0;
        ui->checkBox_2->setCheckState(Qt::CheckState::Unchecked);
        this->setStyleSheet("background-color: rgb(255,255,255);");
    } else {
        ui->checkBox_2->setCheckState(Qt::CheckState::Checked);
    }
}

void menuThemeWindow::on_checkBox_2_stateChanged(int arg1)
{
    if (arg1 == Qt::CheckState::Checked) {
        _theme = 1;
        ui->checkBox->setCheckState(Qt::CheckState::Unchecked);
        this->setStyleSheet("background-color: rgb(171,171,171);");
    } else {
        ui->checkBox->setCheckState(Qt::CheckState::Checked);
    }
}

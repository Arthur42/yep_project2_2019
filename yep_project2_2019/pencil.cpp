#include "pencil.h"
#include "ui_mainwindow.h"

Pencil::Pencil()
{
    _square = false;
}

void Pencil::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    (void)colorLayer;
    if (_size <= 0) return;
    int minX = x - _size / 2;
    int minY = y - _size / 2;
    (_square) ? this->print_square(image, minX, minY) : this->print_circle(image, minX, minY);
}

void Pencil::getToolBox(QFormLayout *layout)
{
    _boxSize = new QLineEdit();
    _boxColor = new QPushButton();
    _boxCheck = new QCheckBox();

    removeOldToolBox(layout);

    _boxSize->setValidator(new QIntValidator);
    _boxSize->setText(QString(std::to_string(_size).c_str()));
    _boxColor->setText("");
    _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(_color.red()) +
                                         ", " + std::to_string(_color.green()) +
                                         ", " + std::to_string(_color.blue()) +
                                         ", " + std::to_string(_color.alpha()) + ")").c_str());
    _boxCheck->setText("");
    _boxCheck->setStyleSheet("QCheckBox::indicator { width:25px; height: 25px;}");
    _boxCheck->setCheckState((_square) ? Qt::Checked : Qt::Unchecked);

    connect(_boxSize, SIGNAL(returnPressed()), this, SLOT(on_size_editingFinished()));
    connect(_boxColor, SIGNAL(clicked()), this, SLOT(on_color_clicked()));
    connect(_boxCheck, SIGNAL(stateChanged(int)), this, SLOT(on_check_clicked(int)));

    layout->addRow("Taille", _boxSize);
    layout->addRow("Couleur", _boxColor);
    layout->addRow("Forme carré", _boxCheck);
}

void Pencil::on_check_clicked(int state)
{
    _square = (state == 0) ? false : true;
}

void Pencil::print_square(QImage *image, int x, int y)
{
    QPainter painter(image);

    painter.fillRect(x, y, _size, _size, _color);
}

void Pencil::print_circle(QImage *image, int x, int y)
{
    QPainter painter(image);
    QPen pen;

    pen.setStyle(Qt::NoPen);
    painter.setPen(pen);
    painter.setBrush(QBrush(_color));
    painter.drawEllipse(x, y, _size, _size);
}

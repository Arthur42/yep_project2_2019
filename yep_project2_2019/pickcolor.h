#ifndef PICKCOLOR_H
#define PICKCOLOR_H

#include "atool.h"
#include <QPushButton>
#include <QColorDialog>

class PickColor: public ATool
{
    Q_OBJECT;
public:
    PickColor();
    ~PickColor();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) override;
    void getToolBox(QFormLayout *layout) override;

private slots:
    void on_color_clicked();

protected:
    QPushButton *_boxColor;
};

#endif // PICKCOLOR_H

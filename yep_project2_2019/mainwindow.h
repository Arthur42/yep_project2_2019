#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QColorDialog>
#include <QImage>
#include <QMouseEvent>
#include <QLineEdit>
#include <QIntValidator>
#include <QMenuBar>
#include <QAction>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QBuffer>
#include <QToolButton>
#include <QSizePolicy>
#include <map>
#include <iostream>
#include <vector>
#include <QPrinter>
#include <QPrintDialog>
#include <QClipboard>

#include "ui_mainwindow.h"
#include "project.h"
#include "layer.h"

class ATool;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void menuAbout();
    void menuTheme();
    void on_pushButton_clicked();
    void on_picture_clicked(QMouseEvent *even);
    void on_picture_release();
    void on_picture_move(QMouseEvent *even);
    void on_paintbrush_tool_clicked();
    void on_default_tool_clicked();
    void on_pencil_tool_clicked();
    void on_eraser_tool_clicked();
    void on_pickcolor_tool_clicked();
    void on_create_project_clicked();
    void on_open_project_clicked();
    void on_save_project_clicked();
    void printImage();
    void printImagePDF();
    void resize();
    void rotate();
    void clipboardPasteImage();
    void on_eye_clicked();
    void on_lock_clicked();
    void on_new_layer_clicked();
    void on_layer_clicked();
    void on_export_btn_clicked();
    void fullscreen();
    void on_fill_tool_clicked();

private:
    void setMenu();
    void uncheckButtons();
    void saveToJson();
    void draw_on_picture(int x, int y);
    void update_picture();
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void closeEvent(QCloseEvent *e);
    void createLayerRow(const QString &name, bool isLayer, bool isActif);
    void createLayer(Layer &layer, bool isNew = true);
    bool deleteLayer(int index);
    void clearProject();
    Ui::MainWindow *ui;
    QMenu *_menu;
    QMenu *_menuZoom;
    QMenu *_menuMR;
    QMenu *_menuCM;
    QMenu *_menuMode;
    QMenu *_menuAccu;
    QMenu *_menuCM2;
    QMenu *_menuTransform;
    QMenu *_menuGuide;
    QMenu *_menuMeta;
    QMenu *_menuOS;
    QMenu *_menuOP;
    QMenu *_menuOT;
    QMenu *_menuFA;
    bool _clicked;
    int _lastXPos;
    int _lastYPos;
    bool _imageSet;
    std::string _toolName;
    std::map<std::string, ATool *> _tools;
    int _theme;
    Project _currentProject;
    bool _crtlPressed;
    bool _saved;
    std::vector<Layer> _layers;
    MyQLabel *_selectedLayerName;
    bool _fullscreen;
};
#endif // MAINWINDOW_H

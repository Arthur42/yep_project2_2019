#ifndef FILLTOOL_H
#define FILLTOOL_H

#include "atool.h"
#include <QPushButton>
#include <QLineEdit>
#include <QPainter>
#include <QIntValidator>
#include <QColorDialog>

class FillTool: public ATool
{
    Q_OBJECT;
public:
    FillTool();
    ~FillTool();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) override;
    void getToolBox(QFormLayout *layout) override;

protected slots:
    void on_color_clicked();
    void fillPixelCol(QImage *, int);
    void fillPixelLine(QImage *, int, int);

protected:
    QPushButton *_boxColor;
    QColor _colorToReplace;
    int _imageWidth;
    int _imageHeight;
};

#endif // FILLTOOL_H

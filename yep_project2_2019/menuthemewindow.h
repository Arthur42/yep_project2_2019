#ifndef MENUTHEMEWINDOW_H
#define MENUTHEMEWINDOW_H

#include <QDialog>

namespace Ui {
class menuThemeWindow;
}

class menuThemeWindow : public QDialog
{
    Q_OBJECT

public:
    explicit menuThemeWindow(QWidget *parent = nullptr);
    ~menuThemeWindow();
    void setTheme(int);
    int getTheme();

private slots:
    void on_checkBox_stateChanged(int arg1);

    void on_checkBox_2_stateChanged(int arg1);

private:
    Ui::menuThemeWindow *ui;
    int _theme;
};

#endif // MENUTHEMEWINDOW_H

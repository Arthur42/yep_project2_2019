#include "paintbrush.h"
#include "ui_mainwindow.h"

PaintBrush::PaintBrush()
{
}

PaintBrush::~PaintBrush()
{}

void PaintBrush::useTool(QImage *image, int x, int y, const QColor &colorLayer)
{
    (void)colorLayer;
    if (_size <= 0) return;
    int minX = (x - _size / 2 > 0) ? x - _size / 2 : 0;
    int minY = (y - _size / 2 > 0) ? y - _size / 2 : 0;
    QPainter painter(image);
    QRadialGradient radialGrad(QPointF(x, y), _size);
    QRect rect_radial(minX, minY,_size,_size);

    radialGrad.setColorAt(0, QColor(_color.red(), _color.green(), _color.blue(), 255));
    radialGrad.setColorAt(0.05, QColor(_color.red(), _color.green(), _color.blue(), 220));
    radialGrad.setColorAt(0.1, QColor(_color.red(), _color.green(), _color.blue(), 200));
    radialGrad.setColorAt(0.2, QColor(_color.red(), _color.green(), _color.blue(), 150));
    radialGrad.setColorAt(0.3, QColor(_color.red(), _color.green(), _color.blue(), 100));
    radialGrad.setColorAt(0.4, QColor(_color.red(), _color.green(), _color.blue(), 50));
    radialGrad.setColorAt(0.5, QColor(_color.red(), _color.green(), _color.blue(), 0));
    radialGrad.setColorAt(0.6, QColor(_color.red(), _color.green(), _color.blue(), 0));
    radialGrad.setColorAt(0.7, QColor(_color.red(), _color.green(), _color.blue(), 0));
    radialGrad.setColorAt(0.8, QColor(_color.red(), _color.green(), _color.blue(), 0));
    radialGrad.setColorAt(0.9, QColor(_color.red(), _color.green(), _color.blue(), 0));
    radialGrad.setColorAt(1, QColor(_color.red(), _color.green(), _color.blue(), 0));

    painter.fillRect(rect_radial, radialGrad);
}

void PaintBrush::getToolBox(QFormLayout *layout)
{
    _boxSize = new QLineEdit();
    _boxColor = new QPushButton();

    removeOldToolBox(layout);

    _boxSize->setValidator(new QIntValidator);
    _boxSize->setText(QString(std::to_string(_size).c_str()));
    QColor c = _color;
    _boxColor->setText("");
    _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(c.red()) + ", " + std::to_string(c.green()) + ", " + std::to_string(c.blue()) + ", " + std::to_string(c.alpha()) + ")").c_str());
    connect(_boxSize, SIGNAL(returnPressed()), this, SLOT(on_size_editingFinished()));
    connect(_boxColor, SIGNAL(clicked()), this, SLOT(on_color_clicked()));
    layout->addRow("Taille", _boxSize);
    layout->addRow("Couleur", _boxColor);
}

void PaintBrush::on_size_editingFinished()
{
    QString str = _boxSize->text();
    int value = str.toInt();

    _boxSize->clearFocus();
    _size = value;
}

void PaintBrush::on_color_clicked()
{
    QColor c = QColorDialog::getColor(_color, this, "Choose a color");

    if (c.isValid()) {
        _color = c;
        _boxColor->setStyleSheet(std::string("background-color: rgba(" + std::to_string(c.red()) + ", " + std::to_string(c.green()) + ", " + std::to_string(c.blue()) + ", " + std::to_string(c.alpha()) + ")").c_str());
    }
}

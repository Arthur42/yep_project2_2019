#ifndef PROJECT_H
#define PROJECT_H

#include <QColor>
#include <QJsonObject>

class Project
{
public:
    Project();
    Project(QString &name, int width, int height, bool isTransparent, QColor &color);
    QString getName() const;
    void setName(QString name);
    int getWidth() const;
    void setWidth(int width);
    int getHeight() const;
    void setHeight(int height);
    bool isTransparent() const;
    void setTransparent(bool status);
    QColor getColor() const;
    void setColor(const QColor &color);
    void writeInJSon(QJsonObject &obj);
private:
    QString _name;
    int _width;
    int _height;
    bool _isTransparent;
    QColor _color;
};

#endif // PROJECT_H

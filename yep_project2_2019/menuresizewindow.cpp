#include "menuresizewindow.h"
#include "ui_menuresizewindow.h"

menuResizeWindow::menuResizeWindow(int w, int h, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuResizeWindow)
{
    ui->setupUi(this);
    ui->width->insert(QString::number(w));
    ui->height->insert(QString::number(h));
    _h = h;
    _w = w;
}

menuResizeWindow::~menuResizeWindow()
{
    delete ui;
}

QSize menuResizeWindow::getSize()
{
    return QSize(_w, _h);
}

void menuResizeWindow::on_yes_clicked()
{
    _w = std::stod(ui->width->text().toStdString());
    _h = std::stod(ui->height->text().toStdString());
    close();
}

void menuResizeWindow::on_no_clicked()
{
    close();
}

#ifndef ATOOL_H
#define ATOOL_H

#include <QColor>
#include <QImage>
#include <QLayout>
#include <QWidget>
#include <QFormLayout>

class ATool: public QWidget
{
    Q_OBJECT
public:
    ATool();
    ~ATool();
    void setSize(int size);
    int getSize(void) const;
    void setColor(QColor color);
    QColor getColor(void) const;
    virtual void useTool(QImage *image, int x, int y, const QColor &colorLayer) = 0;
    virtual void getToolBox(QFormLayout *layout) = 0;

protected:
    void removeOldToolBox(QFormLayout *layout);
    int _size;
    QColor  _color;
};

#endif // ATOOL_H

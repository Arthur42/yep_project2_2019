#ifndef LAYERINFORMATION_H
#define LAYERINFORMATION_H

#include <QDialog>
#include <QMessageBox>

#include "layer.h"

namespace Ui {
class LayerInformation;
}

class LayerInformation : public QDialog
{
    Q_OBJECT

public:
    explicit LayerInformation(QWidget *parent = nullptr);
    LayerInformation(const Layer &layer, QWidget *parent = nullptr);
    ~LayerInformation();
    bool toDelete() const;

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::LayerInformation *ui;
    bool _toDelete;
};

#endif // LAYERINFORMATION_H

#ifndef PAINTBRUSH_H
#define PAINTBRUSH_H

#include "atool.h"
#include <QPushButton>
#include <QLineEdit>
#include <QPainter>
#include <QIntValidator>
#include <QColorDialog>

class PaintBrush: public ATool
{
    Q_OBJECT;
public:
    PaintBrush();
    ~PaintBrush();
    void useTool(QImage *image, int x, int y, const QColor &colorLayer) override;
    void getToolBox(QFormLayout *layout) override;

protected slots:
    void on_size_editingFinished();
    void on_color_clicked();

protected:
    QLineEdit *_boxSize;
    QPushButton *_boxColor;
};

#endif // PAINTBRUSH_H
